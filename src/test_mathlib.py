# Project: Calculator

##
# @file
# @brief Test for custom mathematical library (test driven development)
#
# @author xjanec31 Martin Janecek
# @author xkucer0i Alzbeta Kucerova
# @author xcesko00 Simona Ceskova
# @author xdoeri00 Erika Do
#

import mathlib
import unittest

class TestMath(unittest.TestCase):
    
    def setUp(self):
        """!Sets up a sequence of values for additional testing."""

        self.values = [8,4,2,1]


    def test_add(self):
        """!Tests the add() function."""

        self.assertEqual(mathlib.add(2,3), 5)
        self.assertEqual(mathlib.add(0,8), 8)
        self.assertEqual(mathlib.add(-5,-25), -30)
        self.assertEqual(mathlib.add(-5,7), 2)
        
        result = 0

        for i in self.values:
            #adds up all four elements
            result = mathlib.add(result, i)

        self.assertEqual(result, 15)


    def test_substract(self):
        """!Tests the substract() function."""

        self.assertEqual(mathlib.substract(100,80), 20)
        self.assertEqual(mathlib.substract(-5,9), -14)
        self.assertEqual(mathlib.substract(1,1), 0)
        self.assertEqual(mathlib.substract(-5,-5), 0)

        result = self.values[0]

        for i in self.values[1:]:
            #substracts remaining three elements from the first one
            result = mathlib.substract(result, i)

        self.assertEqual(result, 1)


    def test_multiply(self):
        """!Tests the multiply() function."""

        self.assertEqual(mathlib.multiply(1,5), 5)
        self.assertEqual(mathlib.multiply(-1,5), -5)
        self.assertEqual(mathlib.multiply(-2,-5), 10)
        self.assertEqual(mathlib.multiply(0,0), 0)

        result = 1

        for i in self.values:
            #multiplies all four elements
            result = mathlib.multiply(result, i)

        self.assertEqual(result, 64)


    def test_divide(self):
        """!Tests the divide() function."""

        self.assertEqual(mathlib.divide(100,20), 5)
        self.assertEqual(mathlib.divide(-1,2), -0.5)
        self.assertEqual(mathlib.divide(-4,-4), 1)
        self.assertEqual(mathlib.divide(8,-2), -4)

        with self.assertRaises(ValueError):
            mathlib.divide(1,0)

        expected = [0.125,0.25,0.5,1]

        for i in range(len(self.values)):
            #divides 1 by each element separately
            self.assertEqual(mathlib.divide(1,self.values[i]), expected[i])


    def test_factorial(self):
        """!Tests the factorial() function."""

        self.assertEqual(mathlib.factorial(4), 24)
        self.assertEqual(mathlib.factorial(0), 1)
    
        with self.assertRaises(ValueError):
            mathlib.factorial(0.5)

        with self.assertRaises(ValueError):
            mathlib.factorial(-4)

        with self.assertRaises(ValueError):
            mathlib.factorial(0.25)

        expected = [40320,24,2,1]

        for i in range(len(self.values)):
            #calculates the factorial of each element
            self.assertEqual(mathlib.factorial(self.values[i]), expected[i])


    def test_exponent(self):
        """!Tests the exponent() function."""

        self.assertEqual(mathlib.exponent(2,2), 4)
        self.assertEqual(mathlib.exponent(8,0), 1)
        self.assertEqual(mathlib.exponent(-1,3), -1)

        for i in range(len(self.values)):
            #calculates 1 to the power of each element separately
            self.assertEqual(mathlib.exponent(1,self.values[i]), 1)


    def test_root(self):
        """!Tests the root() function."""

        self.assertEqual(mathlib.root(4,2), 2)
        self.assertEqual(mathlib.root(0,2), 0)
        self.assertEqual(mathlib.root(125,3), 5)
        self.assertEqual(mathlib.root(-1,3), -1)

        with self.assertRaises(ValueError):
            mathlib.root(-9,2)
        
        with self.assertRaises(ValueError):
            mathlib.root(1,0.5)

        expected = [2.828427125,2,1.414213562,1]

        for i in range(len(self.values)):
            #calculates square root of each element
            #tests accuracy - seems like potential implementation error

            result = mathlib.root(self.values[i],2)
            error = abs(result - expected[i])
            self.assertTrue(error <= 0.0001)
            
    
    def test_modulo(self):
        """!Tests the modulo() function."""

        self.assertEqual(mathlib.modulo(16,4), 0)
        self.assertEqual(mathlib.modulo(14,3), 2)
        self.assertEqual(mathlib.modulo(-6,2), 0)
        self.assertEqual(mathlib.modulo(0,2), 0)

        with self.assertRaises(ValueError):
            mathlib.modulo(0,0)

        for i in self.values:
            #checks remainder after dividing each element by 1
            self.assertEqual(mathlib.modulo(i, 1), 0)

    def test_combiner(self):
        """!Tests the combiner() function."""

        self.assertEqual(mathlib.combiner(list("3+5")), ['3', '+', '5'])
        self.assertEqual(mathlib.combiner(list("1.11-555")), ['1.11', '-', '555'])
        self.assertEqual(mathlib.combiner(list("(11-22)*(3*(4.4/55))")), ['(', '11', '-', '22', ')', '*', '(', '3', '*', '(', '4.4', '/', '55', ')', ')'])
        self.assertEqual(mathlib.combiner(list("31*6")), ['31', '*', '6'])
        self.assertEqual(mathlib.combiner(list("3*16")), ['3', '*', '16'])

    def test_negCheck(self):
        """!Tests the negCheck() function."""

        self.assertEqual(mathlib.negCheck(['-', '3', '*', '-', '9']), ['-3', '*', '-9'])
        self.assertEqual(mathlib.negCheck(['(', '1', '-', '7', ')']), ['(', '1', '-', '7', ')'])
        self.assertEqual(mathlib.negCheck(['6']), ['6'])

    def test_caller(self):
        """!Tests the caller() function."""

        self.assertEqual(mathlib.caller(['3', '*', '3']), 9)
        self.assertEqual(mathlib.caller(['1', '+', '4', '*', '2']), 9)
        self.assertEqual(mathlib.caller(['21', '%', '8', '/', '2']), 2.5)
        self.assertEqual(mathlib.caller(['9', '-', '2', '^', '2']), 5)

    def test_parser(self):
        """!Tests the parser() function."""

        self.assertEqual(mathlib.parser(['8', '/', '8']), 1)
        self.assertEqual(mathlib.parser(['2', '^', '(', '4', ')']), 16)
        self.assertEqual(mathlib.parser(['2', '^', '4']), 16)
        self.assertEqual(mathlib.parser(['1', '+', '3', '*', '8', '/', '4']), 7)
        self.assertEqual(mathlib.parser(['(', '-', '3', '-', '(', '-', '9', ')', ')']), 6)
        self.assertEqual(mathlib.parser(['(', '3', '+', '1', ')', '!']), 24)
        self.assertEqual(mathlib.parser(['(', '3', '*', '(', '1', '+', '1', ')', ')', '+', '(', '1', '-', '1', ')']), 6)
        self.assertEqual(mathlib.parser(['1', '0', '-', '5', '%', '5']), 10)
        self.assertEqual(mathlib.parser(['5', '%', '3', '*', '2']), 4)
        self.assertEqual(mathlib.parser(['3', '√', '(', '2', '6', '*', '1', '+', '1', '-', '0', ')']), 3)
        self.assertEqual(mathlib.parser(['1', '+', '6', '√', '(', '6', '0', '+', '1', '+', '3', ')', '*', '2']), 5)
        self.assertEqual(mathlib.parser(['1', '!', '+', '2', '!', '+', '3', '!']), 9)
        self.assertEqual(mathlib.parser(['7', '/', '7', '*', '6']), 6)
        self.assertEqual(mathlib.parser(['(', '1', ')', '*', '(', '8', '9', ')', '+', '(', '7', ')', '+', '3']), 99)
        self.assertEqual(mathlib.parser(['0']), 0)
        self.assertEqual(mathlib.parser(['(', '-', '3', '-', '(', '-', '9', ')', ')']), 6)
        self.assertEqual(mathlib.parser(['(', '√', '4',')']), 2)
        self.assertEqual(mathlib.parser(['(', '2.111111' ,')']), 2.111111)

        result = mathlib.parser(['\u03C0', '+', '12'])
        error = abs(result - 15.14159265)
        self.assertTrue(error < 0.001)

        result = mathlib.parser(['\u03C0', '-', '1', '/', '2'])
        error = abs(result - 2.641592654)
        self.assertTrue(error < 0.001)
        

if __name__ == '__main__':
    unittest.main()