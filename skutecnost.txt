Termíny uvedené v plánu projektu byly nastaveny vcelku rozumně, ale mohlo dojít k posunu vybraných termínů v rámci několika dní. Zejména termín dokončení matematické knihovny s pokročilými funkcemi bylo lepší stanovit o několik dní později, jelikož na kompletaci implementace nebylo dostatek času. Zároveň byl v původním plánu opomenut deadline instalátoru, který byl ovšem v průběhu dodán.

Termíny v projektu byly flexibilně upravovány na základě potřeb a domluvy členů týmu.

Skutečnost mezi plánem a projektem byla až na drobnosti podle našich původních představ.
