# Project: Calculator

##
# @file licence.py
# @brief Licence window for calculator
#
# @author xjanec31 Martin Janecek
# @author xkucer0i Alzbeta Kucerova
# @author xcesko00 Simona Ceskova
# @author xdoeri00 Erika Do


from PyQt5 import QtCore, QtGui, QtWidgets
import webbrowser

class licenceGui(QtWidgets.QMainWindow):
    """!Class for whole licence window"""

    def __init__(self, parent):
        """!The constructor"""

        super(licenceGui, self).__init__(parent)
        self.setObjectName("licence")
        self.setWindowTitle("licence")
        self.resize(400, 400)
        self.setMaximumSize(QtCore.QSize(600, 600))

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("rubber-duckie.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.setWindowIcon(icon)
        self.label = QtWidgets.QLabel(self)
        self.label.setGeometry(QtCore.QRect(20, 30, 331, 41))
        self.label.setObjectName("label")

        self.label_2 = QtWidgets.QLabel(self)
        self.label_2.setGeometry(QtCore.QRect(20, 70, 371, 41))
        self.label_2.setObjectName("label_2")

        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("licence", "licence"))
        self.label.setText(_translate("licence", "This application is under GNU GPL v3.0"))

        self.label_2.setText(_translate("licence", "Icon image made by Pixelmeetup from www.flaticon.com"))

        self.pushButton_LINK_ICON = QtWidgets.QPushButton(self)
        self.pushButton_LINK_ICON.setGeometry(QtCore.QRect(20, 120, 160, 40))
        self.pushButton_LINK_ICON.setObjectName("pushButton_ICON")
        self.pushButton_LINK_ICON.setText("Icon source")
        
        self.pushButton_LINK_ICON.clicked.connect(lambda: self.link_clicked())

    def link_clicked(self):
        """!Open web link with source of icon"""

        webbrowser.open('https://www.flaticon.com/free-icon/rubber-duck_2444732?term=rubber%20duck&page=1&position=15&page=1&position=15&related_id=2444732&origin=search')