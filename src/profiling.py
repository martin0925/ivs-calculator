# Project: Calculator

##
# @file profiling.py
# @brief Profiling
#
# @author xjanec31 Martin Janecek
# @author xkucer0i Alzbeta Kucerova
# @author xcesko00 Simona Ceskova
# @author xdoeri00 Erika Do
#

import math
import sys
import fileinput

def diameter(array, n):
    """!Counting diameter of all numbers.
    @param array Array of numbers
    @param n Number of numbers in array
    @return Diameter of all numbers
    """
    suma = 0
    for i in (array):
        suma = suma + float(i)
    return (suma/n)

def result(array, dmtr_number, n):
    """!Uses formula of sample standard deviation
    @param array Array of numbers
    @param dmtr_number Calculated diameter from diameter()
    @param n Number of numbers in array
    @return The result of sample standard deviation
    """
    suma = 0
    for i in (array):
        suma += (float(i) - dmtr_number)**2
    return (math.sqrt(suma/(n-1)))

def check(inp):
    """!Check if input from stdin is corect - if not, before exit writes error code on stderr
    @param inp Array of characters
    """
    i = 0
    counter_float = 0
    while ( i < len(inp) ):
        if ( inp[i].isdigit() ) or ( inp[i] == '\n' )  or ( inp[i] == ' ' ) or ( inp[i] == '.' ) or ( inp[i] == '-'):            
            if( inp[i] == '.' ) and not ( (inp[i+1].isdigit()) and (inp[i-1].isdigit()) ):   
                exit(sys.stderr.write("Input error\n"))
            if( inp[i] == '-' ) and not ( (inp[i+1].isdigit()) and not (inp[i-1].isdigit()) ):   
                exit(sys.stderr.write("Input error\n"))
            if( inp[i] == '.' ):
                counter_float = counter_float + 1
                if(counter_float > 1):
                    exit(sys.stderr.write("Input error\n"))
            if( inp[i] == '\n') or (inp[i] == ' '):
                counter_float = 0
        else:
            exit(sys.stderr.write("Input error\n"))
        i += 1

def parser(inp):
    """!Parser() takes care of whitespace
    @param inp Array of numbers and whitespaces.
    @return Modified array and whitespaces are replaced with \n
    """
    buffer = []
    i = 0
    while ( i < len(inp) ):
        if ( i != 0 ):
            if ( (inp[i].isdigit()) or  (inp[i] == '-')) and ((inp[i-1] == ' ') or (inp[i-1] == '\n')):
                buffer.append('\n')
        if ( inp[i].isdigit() ) or ( inp[i] == '.' ) or ( inp[i] == '-'):
            buffer.append(inp[i])
        i += 1
    return buffer

def combiner(inp):
    """!Puts number together
    @param inp Array of digits and \n char
    @return Modified array with idited numbers in correct form
    """
    num_parts = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-']
    deleted = 0
    add = 0
    first = False

    for i in range(len(inp) - 1):
        if (i == 0):
            first = True
        i = i - deleted + add
        if (i > len(inp)-2):
            break
        if (all(c in num_parts for c in inp[i])) and (inp[i+1] in num_parts):
            inp[i] = inp[i] + inp[i+1]
            del inp[i+1]
            deleted = deleted + 1
        elif (first == False):
            add = 1
        first = False
    return inp

def delete(array):
    """!Deletes extra \n characters
    @param array Array of numbers and \n char
    @return Modified array only with numbers
    """
    for i in range(len(array) - 1):
        if (array.count('\n') == 0):
            break 

        if (array[i] == '\n'):
            del array[i]
    return array
 

def main():
    """!Main function calls other functions
    """
    inp = sys.stdin.read()
    check(inp)
    array = delete(combiner(parser(inp)))
    n = len(array)
    dmtr_number = diameter(array, n)
    final_result = result(array, dmtr_number, n)
    print(round(final_result, 9))

main() 