# Project: Calculator

##
# @file mathlib.py
# @brief Library for custom calculator
# @author xjanec31 Martin Janecek
# @author xkucer0i Alzbeta Kucerova
# @author xcesko00 Simona Ceskova
#@author xdoeri00 Erika Do
#

def add(x, y):
    """!Takes two variables and summarizes them.
    @param x First summand of the operation.
    @param y Second summand of the operation.
    @return Sum of two numbers.
    """
    return x + y

def substract(x, y):
    """!Takes two variables and deducts one number from the other.
    @param x Minuend alias the number that it is substracted from.
    @param y Subtrahend alias the number that is being substracted.
    @return Difference of two numbers.
    """
    return x - y

def multiply(x, y):
    """!Takes two variables and multiplies them.
    @param x Multiplicand alias the number to be multiplied.
    @param y Multiplier alias the number by which it is multiplied.
    @return Product of two numbers.
    """
    return x * y

def divide(x, y):
    """!Takes two variables and divides first number by the second one.
    @param x Dividend alias the number that is being divided.
    @param y Divisor alias the number by which it is divided.
    @exception ValueError Raised when y is zero.
    @return Quotient of two numbers.
    """
    if (y == 0):
        raise ValueError("Cannot divide by zero!")
    else:
        return x / y

def factorial(x):
    """!Returns the product of all positive integers less than or equal to the x.
    @param x X is the first number of sequence which is continues with multiplication of numbers that are decreased by one.
    @exception ValueError Raised when x is not a whole number or is a negative number.
    @return Result of the multiplied number sequence.
    """
    if (isinstance(x, int) != True):
        raise ValueError("Value has to be an integer!")
    elif (x < 0):
        raise ValueError("Cannot be a negative number!")
    elif (x == 0 or x == 1):
        return 1
    elif (x > 69):
        raise ValueError("69! is the higgest countable factorial!")
    else:
        return x * factorial(x - 1)
    
def exponent(x, n):
    """!Takes two variables and returns an exponential of x to the power of n.
    @param x Base alias the number that is being raised.
    @param n Exponent alias the amount of times by which x will be multiplied by itself.
    @exception ValueError Raised when n is not an integer.
    @return Result of n times multiplication of operand x.
    """
    if (isinstance(n, int) != True):
        raise ValueError("Exponent has to be an integer!")
    return x ** n
    
def round(x):
    """!Takes a variable and converts it to the integer.
    @param x The number that is being rounded.
    @return Result of the rounded number.
    """
    if (x > 0):
        x += 0.5
        x = int(x)
        return x
    else:
        x -= 0.5
        x = int(x)
        return x

def root(x, n):
    """!Takes two variables and returns an exponential of x to the power of (1 / n).
    @param x Base alias the number that is being raised.
    @param n The number that will be used as an exponent, but in fraction (1 / n).
    @exception ValueError Raised when x is a negative number and n is an even number or when n is not an integer.
    @return Result of square root of the x.
    """
    if (isinstance(n, int) != True):
        raise ValueError("Exponent has to be an integer type!")
    elif ((x < 0) & (n % 2 == 0)):
        raise ValueError("Cannot root negative numbers!")
    elif (x < 0):
        result = (-x) ** (1/n)
        result = -result
        return result
    else:
        result = x ** (1/n) * 1000000000
        result = round(result)
        result = result / 1000000000
        return result

def modulo(x, y):
    """!Takes two variables and returns a remainder of their division.
    @param x Dividend alias the number that is being divided.
    @param y Divisor alias the number by which it is divided.
    @exception ValueError Raised when y is a zero.
    @return Remainder of the x divided by y.
    """
    if (y == 0):
        raise ValueError("Cannot divide by zero!")
    else:
        return x % y

def combiner(math_exp):
    """!Turns elements (digits and decimal points) in sequence into single element of given list.
    @param math_exp List of disjointed numbers, operators and brackets.
    @return List of numbers (each number's parts connected into single element), operators and brackets.
    """
    num_parts = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
    deleted = 0

    if (type(math_exp) == str):
        array = math_exp.split('*10^')
        math_exp = []
        exp = exponent(10, int(array[1]))
        math_exp.append(str(multiply(float(array[0]), exp)))

    count = math_exp.count('\u03C0')
    while (count != 0):
        index = math_exp.index('\u03C0')
        math_exp[index] = str(3.14159265358979)
        count = count - 1

    for i in range(len(math_exp) - 1):

        i = i - deleted

        if (i > len(math_exp)-2):
            break

        if (all(c in num_parts for c in (math_exp[i]))) and (math_exp[i+1] in num_parts):
            math_exp[i] = math_exp[i] + math_exp[i+1]
            del math_exp[i+1]
            deleted = deleted + 1
 
    return math_exp

def bracketCounter (math_exp):
    """!Counting pairs of brackets in given list of expressions.
    @param math_exp List of disjointed numbers, operators and brackets.
    @return Number of bracket pairs.
    """
    bracket_cnt = 0
    for i in range(len(math_exp) - 1):
        if (math_exp[i] == '('):
            bracket_cnt = bracket_cnt + 1

    return bracket_cnt

def negCheck(math_exp):
    """!Turns elements and minus in sequence into single element of given list.
    @param math_exp List of disjointed numbers, operators and brackets.
    @return List of numbers (each number's parts connected into single element), operators and brackets.
    """
    operators = ['+', '*', '/', 'root', '^', '-']
    brackets = ['(', ')']
    all_operators = ['+', '*', '/', 'root', '^', '-', '(', ')']
    
    for i in range(len(math_exp) - 1):
        if (i > len(math_exp)-2):
            break

        if (math_exp[0] == '-'):
            math_exp[0] = math_exp[0] + str(math_exp[1])
            del math_exp[1]
        elif (math_exp[i] == '-'):
            if (((math_exp[i-1] in all_operators) == False) and ((math_exp[i+1] in all_operators) == False)):
                break

            if ((math_exp[i+1] in brackets) == False) and ((math_exp[i-1] in brackets) == False):
                math_exp[i] = math_exp[i] + math_exp[i+1]
                del math_exp[i+1]
            elif (math_exp[i-1] in operators):
                math_exp[i] = math_exp[i] + math_exp[i+1]
                del math_exp[i+1]
            
    return math_exp

def numCheck(math_exp): 
    """!Checks if the given value isn't larger than calculator highest standard number.
    @param math_exp Given value.
    @exception ValueError Raised when the value is larger than CHSN (calculator highest standard number).
    @return @b 1 If the ValueError is raised.
    @return @b 0 If value is lower than CHSN (calculator highest standard number).
    """
    if (math_exp > 9.99999999*(10**99)):
        raise ValueError("Result is higher than calculator highest standard number that is 9.99999999*(10^99)!")
        return 1

    return 0 

def caller(math_exp):
    """!Calls basic mathematical functions based on operator.
    @param math_exp List of disjointed numbers, operators and brackets.
    @return Result of mathematical expressions.
    """
    math_exp=(negCheck(combiner(math_exp)))
    operators = ['(', ')', '!', '\u221A', '^', '*', '/', '%', '-', '+']
    while (len(math_exp) > 1):
        count = math_exp.count('!')
        while (count != 0):
            index = math_exp.index('!')
            math_exp[index-1] = float(math_exp[index-1])
            math_exp[index-1] = factorial(int(math_exp[index-1]))
            del math_exp[index]
            count = count - 1

        while (math_exp.count('^') != 0 or math_exp.count('\u221A') != 0):
            for i in range(len(math_exp) - 1):
                if (i > len(math_exp)-1):
                    break

                if (math_exp[i] == '^'):
                    math_exp[i+1] = float(math_exp[i+1])
                    math_exp[i-1] = exponent(float(math_exp[i-1]), int(math_exp[i+1]))
                    numCheck(float(math_exp[i-1]))
                    del math_exp[i]
                    del math_exp[i]
                    i = 0
                elif (math_exp[i] == '\u221A' ):
                    if (i == 0):
                        math_exp.insert(i, '\u221A')
                        math_exp[0] = '2'
                        math_exp[i] = root(float(math_exp[i+2]), int(math_exp[i]))
                        numCheck(float(math_exp[i]))
                        del math_exp[i+1]
                        del math_exp[i+1]
                        i = 0
                    elif (i > 0) and (math_exp[i-1] in operators):
                        math_exp.insert(i, '2')
                        math_exp[i] = root(float(math_exp[i+2]), int(math_exp[i]))
                        numCheck(float(math_exp[i]))
                        del math_exp[i+1]
                        del math_exp[i+1]
                        i = 0
                    elif ((math_exp[i-1] in operators) == False):
                        math_exp[i-1] = root(float(math_exp[i+1]), int(math_exp[i-1]))
                        numCheck(float(math_exp[i-1]))
                        del math_exp[i]
                        del math_exp[i]
                        i = 0

        while (math_exp.count('*') != 0 or math_exp.count('/') != 0 or math_exp.count('%') != 0):
            for i in range(len(math_exp) - 1):
                if (i > len(math_exp)-1):
                    break

                if (math_exp[i] == '*' ):
                    math_exp[i-1] = multiply(float(math_exp[i-1]), float(math_exp[i+1]))
                    numCheck(float(math_exp[i-1]))
                    del math_exp[i]
                    del math_exp[i]
                    i = 0
                elif (math_exp[i] == '/' ):
                    math_exp[i-1] = divide(float(math_exp[i-1]), float(math_exp[i+1]))
                    numCheck(float(math_exp[i-1]))
                    del math_exp[i]
                    del math_exp[i]
                    i = 0
                elif (math_exp[i] == '%' ):
                    math_exp[i-1] = modulo(float(math_exp[i-1]), int(math_exp[i+1]))
                    numCheck(float(math_exp[i-1]))
                    del math_exp[i]
                    del math_exp[i]
                    i = 0
        
        while (math_exp.count('+') != 0 or math_exp.count('-') != 0):
            for i in range(len(math_exp) - 1):
                if (i > len(math_exp)-1):
                    break

                if (math_exp[i] == '+' ):
                    math_exp[i-1] = add(float(math_exp[i-1]), float(math_exp[i+1]))
                    numCheck(float(math_exp[i-1]))
                    del math_exp[i]
                    del math_exp[i]
                    i = 0
                elif (math_exp[i] == '-' ):
                    math_exp[i-1] = substract(float(math_exp[i-1]), float(math_exp[i+1]))
                    numCheck(float(math_exp[i-1]))
                    del math_exp[i]
                    del math_exp[i]
                    i = 0

    return float(math_exp[0])

def round9Dec(math_exp):
    """!Replaces a number with an approximate value that has shorter representation.
    @param math_exp Given value.
    @return Value that is rounded to nine decimal places.
    """
    math_exp = math_exp * 1000000000
    math_exp = round(math_exp)
    return math_exp / 1000000000

def scientific(math_exp):
    """!Checks if the given value is in E-Notation.
    Number in E-Notation form is converted to Scientific Notation.
    @param math_exp Given mathematical expression.
    @return Value in Scientific Notation.
    """
    string = str(math_exp)
    
    if (string.count('e') != 0):
        if (string.count('+') != 0):
            array = string.split('e+')
        else:
            array = string.split('e')

        array[0] = round9Dec(float(array[0]))
        return ("{}*10^{}".format(array[0], array[1]))

    else:
        return round9Dec(float(math_exp))   

def str_helper(string):
    """!Takes a string round the decimal part to nine decimal places.
    @param string Given string with number.
    @return Number with concatenated whole number part and decimal part.
    """
    if (string.count('-') == 0):
        if (string.count('.') != 0):
            dec_places = '0.' + string[2:]
        else:
            dec_places = '0.' + string[1:]
            
        dec_places = round9Dec(float(dec_places))
        dec_places = str(dec_places)
        return string[:1] + '.' + dec_places[2:]
    else:
        dec_places = '0.' + string[2:]
        dec_places = round9Dec(float(dec_places))
        dec_places = str(dec_places)
        return string[:2] + '.' + dec_places[2:]

def parser(math_exp):
    """!Basic function that parses mathematical expressions to sub expressions.
    Calls caller to calculate sub expressions and returns an integer.
    @param math_exp List of disjointed numbers, operators and brackets.
    @return Result of mathematical expressions.
    """
    math_exp=(negCheck(combiner(math_exp)))
    brackets=bracketCounter(math_exp)

    if (len(math_exp) == 1):
        if (float(math_exp[0]) == 0):
            return 0
        elif (float(math_exp[0]) >= (10**16)): 
            length = len(math_exp[0])
            string = str_helper(str(math_exp[0]))
            return string + '*10^' + str(length-1)
        elif (float(math_exp[0]) <= (10**-16)):
            length = len(math_exp[0])
            string = str_helper(str(math_exp[0]))
            if (string.count('-') != 0):
                return string + '*10^' + str(length-3)
            else:
                return string + '*10^-' + str(length-3)

    while (len(math_exp) > 1):
        
        counter = 0
        # i is increased every time in the beginning of the while loop
        # so i starts at index 0
        i = -1 
        while (i < len(math_exp)):
            i = i + 1
            if (brackets > 0 ):

                if ( math_exp[i] == '(' ):

                    counter =  counter + 1
                    if ( counter == brackets ):

                        brackets = brackets - 1
                        buffer = []
                        while ( math_exp[i+1] != ')' ):
                            buffer.append(math_exp[i+1])
                            del math_exp[i+1]
                        del math_exp[i+1]

                        counter = 0
                        math_exp[i] = str(caller(buffer))
                        i = -1

                        if (len(math_exp) == 1):
                            return float(math_exp[0])

            elif (len(math_exp) > 1):
                math_exp = str(caller(math_exp))
                math_exp = scientific(math_exp)
                return math_exp
    
    numCheck(float(math_exp[0]))
    math_exp[0] = scientific(math_exp[0])
    return float(math_exp[0])
