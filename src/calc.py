# Project: Calculator

##
# @file calc.py
# @brief Graphical user interface for calculator
#
# @author xjanec31 Martin Janecek
# @author xkucer0i Alzbeta Kucerova
# @author xcesko00 Simona Ceskova
# @author xdoeri00 Erika Do
#

from PyQt5 import QtWidgets as Qw
from PyQt5 import QtCore as Qc
import sys

from gui import Ui_gui

from licence import licenceGui
from help import helpGui
from mathlib import parser

class CalculatorWindow(Qw.QMainWindow,Ui_gui):
    """!Main class for calculator gui"""

    def __init__(self):
        """!The constructor"""

        super().__init__()
        self.setupUi(self)

        ## buffer for user input, result will be send to mathlib to solve math formula
        self.displayBuffer = []

        ## array for number in temporary memory
        self.memoryNumber = []

        ## list of binrary operators
        self.list_of_binary = ['+','*','/','%']

        ## list of unary operators
        self.list_of_unary = ['!','-','^','^2', '\u221A']

        ## variable for root
        self.root = '\u221A'

        ## variable for pi
        self.pi = '\u03C0'

        self.init_buttons()

        ## clickable link to licence window
        self.licenceGui = licenceGui(self)
        licenceButton = Qw.QAction("licence, credits", self, triggered=(lambda: self.licenceGui.show()))
        self.menuabout.addAction(licenceButton)

        ## clickable link to help window
        self.helpGui = helpGui(self)
        helpButton = Qw.QAction("help", self, triggered=(lambda: self.helpGui.show()), shortcut = "F1")
        self.menuhelp.addAction(helpButton)


    def init_buttons(self):
        """!Initialize all the buttons on main window of calculator"""

        self.pushButton_00.clicked.connect(lambda: self.btn_digit_pressed('0'))
        self.pushButton_01.clicked.connect(lambda: self.btn_digit_pressed('1'))
        self.pushButton_02.clicked.connect(lambda: self.btn_digit_pressed('2'))
        self.pushButton_03.clicked.connect(lambda: self.btn_digit_pressed('3'))
        self.pushButton_04.clicked.connect(lambda: self.btn_digit_pressed('4'))
        self.pushButton_05.clicked.connect(lambda: self.btn_digit_pressed('5'))
        self.pushButton_06.clicked.connect(lambda: self.btn_digit_pressed('6'))
        self.pushButton_07.clicked.connect(lambda: self.btn_digit_pressed('7'))
        self.pushButton_08.clicked.connect(lambda: self.btn_digit_pressed('8'))
        self.pushButton_09.clicked.connect(lambda: self.btn_digit_pressed('9'))

        self.pushButton_ADD.clicked.connect(lambda: self.btn_binary_operator_pressed('+'))
        self.pushButton_MUL.clicked.connect(lambda: self.btn_binary_operator_pressed('*'))
        self.pushButton_DIV.clicked.connect(lambda: self.btn_binary_operator_pressed('/'))
        self.pushButton_MOD.clicked.connect(lambda: self.btn_binary_operator_pressed('%'))

        self.pushButton_MIN.clicked.connect(lambda: self.btn_unary_operator_pressed('-'))
        self.pushButton_FACT.clicked.connect(lambda: self.btn_unary_operator_pressed('!'))
        self.pushButton_POW2.clicked.connect(lambda: self.btn_unary_operator_pressed('^2'))
        self.pushButton_POWN.clicked.connect(lambda: self.btn_unary_operator_pressed('^'))
        self.pushButton_ROOT.clicked.connect(lambda: self.btn_unary_operator_pressed(self.root))

        self.pushButton_PI.clicked.connect(self.btn_pi)
        self.pushButton_LB.clicked.connect(lambda: self.btn_brackets('('))
        self.pushButton_RB.clicked.connect(lambda: self.btn_brackets(')'))

        # decimal button - PT as point
        self.pushButton_PT.clicked.connect(self.btn_decimal_pressed)

        self.pushButton_DEL.clicked.connect(self.btn_clear_last_char)
        self.pushButton_CE.clicked.connect(self.btn_clear_everything)
        self.pushButton_EQ.clicked.connect(self.solve_expression)

        # operating with memory, could save only number in any format (not operators and special symbols)
        self.pushButton_SAVE.clicked.connect(lambda: self.number_memory('save'))
        self.pushButton_SAVEADD.clicked.connect(lambda: self.number_memory('add'))
        self.pushButton_SAVEREAD.clicked.connect(lambda: self.number_memory('read'))
        self.pushButton_SAVECLEAR.clicked.connect(lambda: self.number_memory('clear'))

    def keyPressEvent(self, event):
        """!Process the input from keyboard
        @param event: Action from keyboard"""

        if(event.key() == Qc.Qt.Key_0):
            self.btn_digit_pressed('0')
        if(event.key() == Qc.Qt.Key_1):
            self.btn_digit_pressed('1')
        if(event.key() == Qc.Qt.Key_2):
            self.btn_digit_pressed('2')
        if(event.key() == Qc.Qt.Key_3):
            self.btn_digit_pressed('3')
        if(event.key() == Qc.Qt.Key_4):
            self.btn_digit_pressed('4')
        if(event.key() == Qc.Qt.Key_5):
            self.btn_digit_pressed('5')
        if(event.key() == Qc.Qt.Key_6):
            self.btn_digit_pressed('6')
        if(event.key() == Qc.Qt.Key_7):
            self.btn_digit_pressed('7')
        if(event.key() == Qc.Qt.Key_8):
            self.btn_digit_pressed('8')
        if(event.key() == Qc.Qt.Key_9):
            self.btn_digit_pressed('9')
        if(event.key() == Qc.Qt.Key_Period):
            self.btn_decimal_pressed()
        if(event.key() == Qc.Qt.Key_Plus):
            self.btn_binary_operator_pressed('+')
        if(event.key() == Qc.Qt.Key_Minus):
            self.btn_unary_operator_pressed('-')
        if(event.key() == Qc.Qt.Key_Slash):
            self.btn_binary_operator_pressed('/')
        if(event.key() == Qc.Qt.Key_Asterisk):
            self.btn_binary_operator_pressed('*')
        if(event.key() == Qc.Qt.Key_Percent):
            self.btn_binary_operator_pressed('%')
        if(event.key() == Qc.Qt.Key_Exclam):
            self.btn_unary_operator_pressed('!')
        if(event.key() == Qc.Qt.Key_ParenLeft):
            self.btn_brackets('(')
        if(event.key() == Qc.Qt.Key_ParenRight):
            self.btn_brackets(')')
        if(event.key() == Qc.Qt.Key_Backspace):
            self.btn_clear_last_char()
        if(event.key() == Qc.Qt.Key_Delete):
            self.btn_clear_everything()

        # key event, that open the gui help (when button 'H' is pressed on keyboard)
        if(event.key() == Qc.Qt.Key_H):
            self.helpGui.show()

        # call solve_expression, that will call parser() from mathlib
        if(event.key() == Qc.Qt.Key_Enter or event.key() == Qc.Qt.Key_Return or event.key() == Qc.Qt.Key_Equal):
            self.solve_expression()

        # special key events
        if(event.key() == Qc.Qt.Key_F):
            self.displayBuffer.clear()
            self.displayBuffer.append("PRESS F TO PAY RESPECTS TO ALL DUCKIES")
            self.update_display()

        if(event.key() == Qc.Qt.Key_I):
            self.displayBuffer.clear()
            formula = ['√', '(', '6', ')', '+', '9', '√', '(', '4', '2', ')', '*', '(', '(', '9', '9', '*', '3', ')', '+', '-', '9', '.', '3', '-', '9', '+', '4', '!', '+', '3', '*', '2', '^', '0', ')', '*', '7.42', '+', '5.213772904']
            for i in formula:
                self.displayBuffer.append(i)
            self.update_display()

        if(event.key() == Qc.Qt.Key_Question):
            self.displayBuffer.clear()
            self.displayBuffer.append('4')
            self.displayBuffer.append('2')
            self.update_display()
            self.error_label.setText("The Answer to the Ultimate Question of Life, the Universe and Everything")


    def btn_digit_pressed(self, number):
        """!One of the 9 digits was pressed
        @param number: Digit 0-9"""

        # prevents more nulls at begginning of expression
        if(number == '0'):
            if(len(self.displayBuffer) == 1):
                if(self.displayBuffer[0] == '0'):
                    return

        if(len(self.displayBuffer) > 0):
            if(self.displayBuffer[-1] in [self.pi, ')', '!']):
                self.displayBuffer.append('*')
            if(self.displayBuffer[-1] == self.root):
                self.displayBuffer.append('(')
 
        self.displayBuffer.append(number)
        self.update_display()

    def btn_binary_operator_pressed(self, operator):
        """!Some binary operator was pressed (+,*,/,%)
        @param operator Type of binary operator"""

        if(len(self.displayBuffer) == 0):
            self.error_msg("write a number first")
            return

        if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] in ['-', '.', '(', '^', self.root]):
            return

        self.displayBuffer.append(operator)
        self.update_display()

    def btn_unary_operator_pressed(self, operator):
        """!Some unary operator was pressed (!,-,x^2,x^n,root)
        @param operator Type of unary operator"""

        if(len(self.displayBuffer) == 0 and (operator == '!' or operator == '^' or operator == '^2') ):
            self.error_msg("write a number first")
            return

        if(len(self.displayBuffer) > 0):
            if(self.displayBuffer[-1] == '.'):
                return

        if(operator == '^2'):
            if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] in self.list_of_unary or self.displayBuffer[-1] == '('):
                if(self.displayBuffer[-1] != '!'):
                    return
            self.displayBuffer.append('^')
            self.displayBuffer.append('2')
        elif(operator == '^'):
            if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] in self.list_of_unary or self.displayBuffer[-1] == '('):
                if(self.displayBuffer[-1] != '!'):
                    return
            self.displayBuffer.append(operator)
        elif(operator == '-'):
            if(len(self.displayBuffer) > 0):
                if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] == '^'):
                    self.displayBuffer.append('(')
                if(self.displayBuffer[-1] == '.'):
                    return
                if(self.displayBuffer[-1] == self.root or self.displayBuffer[-1] == '-'):
                    self.displayBuffer.append('(')
            else:
                self.displayBuffer.append('(')
            self.displayBuffer.append(operator)
        elif(operator == '!'):
            if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] in self.list_of_unary or self.displayBuffer[-1] in ['.', '(', self.pi]):
                return
            self.displayBuffer.append(operator)
        elif(operator == self.root):
            if(len(self.displayBuffer) > 0):
                if(self.displayBuffer[-1] == ')' or self.displayBuffer[-1] == '' or self.displayBuffer[-1] == self.pi):
                    self.displayBuffer.append('*')
                if(self.displayBuffer[-1] == self.root):
                    self.displayBuffer.append('(')
            self.displayBuffer.append(operator)
        
        if(operator == self.root):
            self.displayBuffer.append('(')
        
        self.update_display()

    def btn_pi(self):
        """!Prints pi"""

        # append symbol of multiplication, when any number is instantly before pi
        if (len(self.displayBuffer) > 0):
            if (self.displayBuffer[-1].isnumeric() or self.displayBuffer[-1] in [')', '!', self.pi]):
                self.displayBuffer.append('*')
            elif(self.displayBuffer[-1] == self.root):
                self.displayBuffer.append('(')
            elif (self.displayBuffer[-1] == '.'):
                return

        self.displayBuffer.append(self.pi)
        self.update_display()

    def btn_brackets(self, bracketType):
        """!Add brackets to buffer
        @param bracketType Left or right bracket"""

        if(len(self.displayBuffer) > 0):
            if(self.displayBuffer[-1] == '.'):
                return
        if(bracketType == ')' and (len(self.displayBuffer) == 0 or '(' not in self.displayBuffer)):
            self.error_msg("right bracket cannot be first or before any left bracket")
            return

        if(len(self.displayBuffer) > 0):
            if(bracketType == '('):
                if (self.displayBuffer[-1].isnumeric() or self.displayBuffer[-1] in [')', '!', self.pi]):
                    self.displayBuffer.append('*')
            else:
                if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] in ['(', '-']):
                    return

        self.displayBuffer.append(bracketType);
        self.update_display()

    def btn_decimal_pressed(self):
        """!If decimal pressed, prevents multiple decimal points in one expression"""

        if(len(self.displayBuffer) > 0):
            if(self.displayBuffer[-1] in ['(', ')', '^', '!', '.', self.pi]):
                return
            elif(self.displayBuffer[-1] == self.root):
                self.displayBuffer.append('(')
                self.displayBuffer.append('0')
            elif(self.displayBuffer[-1].isnumeric()):
                i = len(self.displayBuffer) -1
                while (self.displayBuffer[i].isnumeric() and i >= 0):
                    i=i-1
                    if(self.displayBuffer[i] == '.'):
                        return

        if(len(self.displayBuffer) == 0):
            self.displayBuffer.append('0')
        else:
            if(self.displayBuffer[-1] in self.list_of_binary or self.displayBuffer[-1] == '-'):
                self.displayBuffer.append('0')

        self.displayBuffer.append('.')
        self.update_display()
        
    def btn_clear_last_char(self):
        """!Clear last char in displayBuffer
        Usage: press the backspace button on keyboard"""

        if(len(self.displayBuffer) != 0):
            self.displayBuffer.pop()
        self.update_display()

    def btn_clear_everything(self):
        """!Clear everything on display and buffer"""

        self.lineEdit.clear()
        self.displayBuffer.clear()

    def number_memory(self, actionType):
        """!Operate with number in temporary memory
        includes only one number in any format
        @param actionType: Type of action for button (save,add,read,clear)"""

        valid = True
        if(actionType == "save"):
            for i in self.displayBuffer:
                if (not i.isnumeric()) and (i != '.'):
                    self.error_msg("you can save only numbers")
                    valid = False
            if (valid):
                self.memoryNumber = self.displayBuffer.copy()
        elif(actionType == "add"):
            if(len(self.memoryNumber) == 0):
                self.memoryNumber.append("0")
            if(len(self.displayBuffer) > 0):
                for i in self.displayBuffer:
                    if (not i.isnumeric()) and (i != '.'):
                        self.error_msg("you can save only numbers")
                        valid = False
                if (valid):
                    memInt = float(self.list_to_string(self.memoryNumber))
                    memInt += float(self.list_to_string(self.displayBuffer))
                    self.memoryNumber.clear()
                    memInt = list(str(memInt))

                    # if number is int, in that case don't need decimal point
                    if memInt[-1] == "0":
                        if memInt[-2] == ".":
                            memInt.pop()
                            memInt.pop()
                    self.memoryNumber = memInt
        elif(actionType == "read"):
            self.btn_clear_everything()
            self.displayBuffer = self.memoryNumber.copy()
            self.update_display()
        elif(actionType == "clear"):
            self.memoryNumber.clear()
        
    def update_display(self):
        """!Updates the display after changing displayBuffer"""

        self.error_label.clear()
        string = self.list_to_string(self.displayBuffer);
        self.lineEdit.setText(string)

    def list_to_string(self, array):
        """!Convert list to string
        @param array List that will be convert to string
        @return string made from array"""

        string = ""
        for i in array:
            string += i
        return(string)
    
    def brackets_counter(self, array):
        """!Count the amount of brackets in array, when odd -> fail
        @return True when number of brackets is OK"""

        counterLeft = 0
        counterRight = 0
        for i in array:
            if i == ')':
                counterRight +=1
            elif i == '(':
                counterLeft +=1

        if ((counterRight + counterLeft) % 2 != 0):
            self.error_msg("number of brackets is odd")
            return(False)

        if (counterRight != counterLeft):
            self.error_msg("number of right brackets and left brackets must be equal")
            return(False)

        return(True)

    def solve_expression(self):
        """!Send displayBuffer to mathlib"""

        brackets_test = self.brackets_counter(self.displayBuffer)
        if (not brackets_test):
            return

        if (len(self.displayBuffer) == 0):
            self.error_msg("type something...")
            return

        # if there's char '.' at the end of buffer, then will be deleted
        if (self.displayBuffer[-1] == '.'):
            self.displayBuffer.pop()
            self.update_display()
        elif (self.displayBuffer[-1] in self.list_of_binary):
            self.error_msg("last operator needs two operands")
            return
        elif (self.displayBuffer[-1] == '-' or self.displayBuffer[-1] == '(' or self.displayBuffer[-1] == '^' or self.displayBuffer[-1] == self.root):
            self.error_msg("invalid last character on display")
            return
        
        # call the parser() function from mathlib to solve expression and convert to string
        result = str(parser(self.displayBuffer))
        self.displayBuffer.clear()
        
        for i in range (len(result)):
            self.displayBuffer.append(result[i])

        # if there's only int number (with ".0" in the end)
        if(len(self.displayBuffer) > 1):
            if (self.displayBuffer[-1] == '0' and self.displayBuffer[-2] == '.'):
                self.displayBuffer.pop()
                self.displayBuffer.pop()

        self.update_display()
    
    def error_msg(self, name):
        """!Prints error message on display
        @param name Extern error message"""

        self.error_label.setText("error: " + name)
        

app = Qw.QApplication(sys.argv)
calculator = CalculatorWindow()
calculator.show()
sys.exit(app.exec_())