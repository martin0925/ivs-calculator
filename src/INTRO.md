
Introduction:
============

This is a Python project meant for the Ubuntu 64bit platform. It is a basic calculator for your basic needs.
See user documentation for details.

Authors:
-------

- Martin Janecek (GitLab: \@martin0925)
- Alzbeta Kucerova (GitLab: \@betulependule)
- Simona Ceskova (GitLab: \@Shalatik)
- Erika Do (GitLab: \@xdoeri00)


Credits:
-------

Icon image made by Pixelmeetup:

https://www.flaticon.com/free-icon/rubber-duck_2444732?term=rubber%20duck&page=1&position=15&page=1&position=15&related_id=2444732&origin=search

License:
-------
This program is a free software. It is distributed under GNU GPL version 3.