In file output is inserted the output of profiling function for calculation of sample standard deviation.
Output values are: 10, 100 and 1000 numbers.
If we want optimize profiling.py code for best performance, the first part to edit should be control function "isdigit" and "len" which is called the most times.
Second part could be in combiner function because there program spends the most time.
This improvement provides better outcome for next development of profiling.
