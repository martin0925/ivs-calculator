# Project: Calculator

##
# @file help.py
# @brief Help for calculator
#
# @author xjanec31 Martin Janecek
# @author xkucer0i Alzbeta Kucerova
# @author xcesko00 Simona Ceskova
# @author xdoeri00 Erika Do


from PyQt5 import QtCore, QtGui, QtWidgets

class helpGui(QtWidgets.QMainWindow):
    """!Class for whole help window"""

    def __init__(self, parent):
        """!The constructor"""

        super(helpGui, self).__init__(parent)
        self.setObjectName("help")
        self.setWindowTitle("help")
        self.setFixedSize(550,550)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("rubber-duckie.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.setWindowIcon(icon)

        self.centered = QtWidgets.QWidget()
        self.helpText = QtWidgets.QTextBrowser(self.centered)
        self.helpText.setGeometry(QtCore.QRect(10,10,530,530))

        self.helpText.setReadOnly(True)

        self.setCentralWidget(self.centered)

        self.helpText.setHtml(
            "<div style=\"margin-left: 15px\">"
                "<h1 style=\"color:#ffc400;\"> Help for calculator </h1>"
                "<h2> Usage of buttons: </h2>"
                "<ul style=\"list-style-type: none\">"
                    "<li style=\"margin-bottom: 10px\"> DEL button - delete last character on display </li>"
                    "<li style=\"margin-bottom: 10px\"> CE button - clear input on display </li>"
                    "<li style=\"margin-bottom: 10px\"> EQUALS button - calculate input and show result on display </li>"
                    "<h3> Memory </h3>"
                    "<ul style=\"list-style-type: square\">"
                        "<li> X➝M - save number into the memory </li>"
                        "<li> M+ - add number to the memory </li>"
                        "<li> MR - read number from the memory </li>"
                        "<li> MC - clear the memory </li>"
                    "</ul>"
                    "<li> <h3> Differences between physical and virtual keyboard </h3> </li>"
                    "<ul style=\"list-style-type: square\">"
                        "<li> DEL button - behaves like CE button on virtual keyboard </li>"
                        "<li style=\"margin-bottom: 10px\"> BACKSPACE button - behaves like DEL button on virtual keyboard  </li>"
                    "</ul>"
                "</ul>"
                "<hr>"
                "<h2> Usage of special key events: </h2>"
                "<ul style=\"list-style-type: none\">"
                    "<li> Button 'F1' on keyboard - open help window </li>"
                    "<li> Button 'H' on keyboard - open help window </li>"
                    "<li> Button 'I' on keyboard - display insane expression </li>"
                    "<li> Button '?' on keyboard - answer to everything </li>"
                    "<li> Button 'F' on keyboard - pay respects </li>"
                "</ul>"
                "<hr>"
                "<h2> Constraints: </h2>"
                "<ul style=\"list-style-type: none\">"
                    "<li> Unable to input multiple factorial symbols '!' in sequence </li>"
                    "<li> Unable to input text characters </li>"
                "</ul>"
                "<br>"
            "</div>"
        )
